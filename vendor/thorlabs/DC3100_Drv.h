/****************************************************************************

	Thorlabs DC3100 driver -  VISA instrument driver

 	Copyright: 	Copyright(c) 2008, Thorlabs GmbH (www.thorlabs.com)
 	Author:		Olaf Wohlmann (owohlmann@thorlabs.com)

	Disclaimer:

	This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


 	Header file

 	Date:       	Apr-22-2008
 	Version:    	1.0

	Changelog:		see 'Readme.rtf'

****************************************************************************/

#ifndef _DC3100_HEADER_
#define _DC3100_HEADER_

#include <vpptype.h>

#if defined(__cplusplus) || defined(__cplusplus__)
extern "C"
{
#endif

/*---------------------------------------------------------------------------
 Buffers
---------------------------------------------------------------------------*/
#define DC3100_BUFFER_SIZE                256      // General buffer size
#define DC3100_ERR_DESCR_BUFFER_SIZE      512      // Error description buffer size

/*---------------------------------------------------------------------------
 Find Pattern
---------------------------------------------------------------------------*/
#define DC3100_FIND_PATTERN		      	"ASRL?*"

/*---------------------------------------------------------------------------
 Error/Warning Codes
---------------------------------------------------------------------------*/
// Offsets
#define VI_INSTR_WARNING_OFFSET                   	(0x3FFC0900L)
#define VI_INSTR_ERROR_OFFSET          (_VI_ERROR + 0x3FFC0900L)	//0xBFFC0900
// Driver Error Codes
#define VI_ERROR_GET_INSTR_ERROR       (_VI_ERROR + 0x3FFC0805L)

#define VI_ERROR_UNKNOWN_ATTRIBUTE		(VI_ERROR_GET_INSTR_ERROR + 1)
#define VI_ERROR_NOT_SUPPORTED			(VI_ERROR_GET_INSTR_ERROR + 2)

/*---------------------------------------------------------------------------
 Register Values
---------------------------------------------------------------------------*/
// common status bits

// common status bits
#define STAT_NO_LED_CHANGED				0x0001
#define STAT_NO_LED							0x0002
#define STAT_VCC_FAIL_CHANGED				0x0004
#define STAT_VCC_FAIL						0x0008
#define STAT_OTP_CHANGED					0x0010
#define STAT_OTP								0x0020
#define STAT_LED_OPEN_CHANGED				0x0040
#define STAT_LED_OPEN						0x0080
#define STAT_LED_LIMIT_CHANGED			0x0100
#define STAT_LED_LIMIT						0x0200
#define STAT_OTP_HEAD_CHANGED				0x0400
#define STAT_OTP_HEAD						0x0800
#define STAT_IFC_REFRESH_CHANGED			0x1000

/*---------------------------------------------------------------------------
 Operation modes
---------------------------------------------------------------------------*/
#define MODUS_CONST_CURRENT		0
#define MODUS_MODULATION			1
#define MODUS_EXTERNAL_CONTROL	2


/*---------------------------------------------------------------------------
 GLOBAL USER-CALLABLE FUNCTION DECLARATIONS (Exportable Functions)
---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Initialize.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_init (ViRsrc resourceName, ViBoolean IDQuery, ViBoolean resetDevice, ViPSession instrumentHandle);

/*---------------------------------------------------------------------------
 Set/Get user limit current.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setLimitCurrent (ViSession instrumentHandle, ViReal32 currentLimit);
ViStatus _VI_FUNC DC3100_getLimitCurrent (ViSession instrumentHandle, ViPReal32 currentLimit);

/*---------------------------------------------------------------------------
 Set/Get maximum current.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setMaxLimit (ViSession instrumentHandle, ViReal32 maximumCurrentLimit);
ViStatus _VI_FUNC DC3100_getMaxLimit (ViSession instrumentHandle, ViPReal32 maximumCurrentLimit);

/*---------------------------------------------------------------------------
 Set/Get maximum frequency.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setMaxFrequency (ViSession instrumentHandle, ViReal32 maximumFrequency);
ViStatus _VI_FUNC DC3100_getMaxFrequency (ViSession instrumentHandle, ViReal32 *maximumFrequency);

/*---------------------------------------------------------------------------
 Set/Get operation mode.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setOperationMode (ViSession instrumentHandle, ViInt32 operationMode);
ViStatus _VI_FUNC DC3100_getOperationMode (ViSession instrumentHandle, ViPInt32 operationMode);

/*---------------------------------------------------------------------------
 Set/Get led output.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setLedOnOff (ViSession instrumentHandle, ViBoolean ledOutput);
ViStatus _VI_FUNC DC3100_getLedOnOff (ViSession instrumentHandle, ViPBoolean ledOutputState);

/*---------------------------------------------------------------------------
 Set/Get internal modulation current.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setModuCurrent (ViSession instrumentHandle, ViReal32 internalModulationCurrent);
ViStatus _VI_FUNC DC3100_getModuCurrent (ViSession instrumentHandle, ViPReal32 internalModulationCurrent);

/*---------------------------------------------------------------------------
 Set/Get internal modulation frequency.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setModuFreq (ViSession instrumentHandle, ViReal32 internalModulationFrequency);
ViStatus _VI_FUNC DC3100_getModuFreq (ViSession instrumentHandle, ViPReal32 internalModulationFrequency);

/*---------------------------------------------------------------------------
 Set/Get internal modulation depth.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setModuDepth (ViSession instrumentHandle, ViInt32 internalModulationDepth);
ViStatus _VI_FUNC DC3100_getModuDepth (ViSession instrumentHandle, ViPInt32 internalModulationDepth);

/*---------------------------------------------------------------------------
 Set/Get constant current.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setConstCurrent (ViSession instrumentHandle, ViReal32 constantCurrent);
ViStatus _VI_FUNC DC3100_getConstCurrent (ViSession instrumentHandle, ViPReal32 constantCurrent);

/*---------------------------------------------------------------------------
 Set/Get the display brightness.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_setDispBright (ViSession instrumentHandle, ViInt32 displayBrightness);
ViStatus _VI_FUNC DC3100_getDispBright (ViSession instrumentHandle, ViPInt32 displayBrightness);

/*---------------------------------------------------------------------------
 Get the status register.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_getStatusRegister (ViSession instrumentHandle, ViPInt32 statusRegister);

/*---------------------------------------------------------------------------
 Get error message.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_errorMessage (ViSession instrumentHandle, ViStatus statusCode, ViChar _VI_FAR message[]);

/*---------------------------------------------------------------------------
 Identification query.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_identificationQuery (ViSession instrumentHandle, ViChar _VI_FAR manufacturerName[], ViChar _VI_FAR deviceName[], ViChar _VI_FAR serialNumber[], ViChar _VI_FAR firmwareRevision[]);

/*---------------------------------------------------------------------------
 Revision query.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_revisionQuery (ViSession instrumentHandle, ViPInt32 instrumentDriverRevision, ViChar firmwareRevision[]);

/*---------------------------------------------------------------------------
 Close.
---------------------------------------------------------------------------*/
ViStatus _VI_FUNC DC3100_close (ViSession instrumentHandle);



#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif

#endif	/* _DC3100_HEADER_ */


/****************************************************************************

End of Header file

****************************************************************************/
