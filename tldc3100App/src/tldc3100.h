/**
 * Asyn driver for the Thorlabs DC3100 - FLIM LED Driver.
 *
 * @author Hinko Kocevar
 * @date July 2016
 *
 */

#ifndef TLDC3100_H
#define TLDC3100_H

#include "asynPortDriver.h"
#include "ftd2xx.h"

#define TLDC3100_BUFFER_SIZE		256

#define TlMessageString			"TL_MESSAGE"
#define TlMnfcString			"TL_MNFC"
#define TlProdString			"TL_PROD"
#define TlSerialString			"TL_SERIAL"
#define TlVIDPIDString			"TL_VIDPID"
#define TlLimitCurrentString	"TL_LIMIT_CURRENT"
#define TlMaxLimitString		"TL_MAX_LIMIT"
#define TlMaxFrequencyString	"TL_MAX_FREQUENCY"
#define TlOpModeString			"TL_OP_MODE"
#define TlLEDControlString		"TL_LED_CONTROL"
#define TlModCurrentString		"TL_MOD_CURRENT"
#define TlModFrequencyString	"TL_MOD_FREQUENCY"
#define TlModDepthString		"TL_MOD_DEPTH"
#define TlConstCurrentString	"TL_CONST_CURRENT"
#define TlDisplayBrightString	"TL_DISPLAY_BRIGHT"
#define TlStatusRegisterString	"TL_STATUS_REGISTER"
#define TlErrorNoLEDString		"TL_ERR_NO_LED"
#define TlErrorVccFailString	"TL_ERR_VCC_FAIL"
#define TlErrorChassisOTPString	"TL_ERR_CHASSIS_OTP"
#define TlErrorLEDOpenString	"TL_ERR_LED_OPEN"
#define TlErrorOverLimitString	"TL_ERR_OVER_LIMIT"
#define TlErrorLEDOTPString		"TL_ERR_LED_OTP"
#define TlErrorRefreshString	"TL_ERR_REFRESH"


/**
 * Driver class for Thorlabs DC3100. This inherits from AsynPortDriver class in asyn.
 *
 */
class TLDC3100 : public asynPortDriver {

public:
	TLDC3100(const char *portName, int usbVID, int usbPID,
			const char *serialNumber, int priority, int stackSize);
	virtual ~TLDC3100();

	/* These are the methods that we use */
	virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus readInt32(asynUser *pasynUser, epicsInt32 *value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual asynStatus readFloat64(asynUser *pasynUser, epicsFloat64 *value);
	virtual void report(FILE *fp, int details);

	// Should be private, but are called from C so must be public
	void deviceTask(void);

protected:
    int TlMessage;
    #define FIRST_COMMAND TlMessage
	int TlMnfc;
	int TlProd;
	int TlSerial;
	int TlVIDPID;
	int TlLimitCurrent;
	int TlMaxLimit;
	int TlMaxFrequency;
	int TlOpMode;
	int TlLEDControl;
	int TlModCurrent;
	int TlModFrequency;
	int TlModDepth;
	int TlConstCurrent;
	int TlDisplayBright;
	int TlStatusRegister;
	int TlErrorNoLED;
	int TlErrorVccFail;
	int TlErrorChassisOTP;
	int TlErrorLEDOpen;
	int TlErrorOverLimit;
	int TlErrorLEDOTP;
	int TlErrorRefresh;
    #define LAST_COMMAND TlErrorRefresh

private:
	void dumpBuffer(char *buffer, unsigned int elements);
	void addFTDevice(void);
	void removeFTDevice(void);
	unsigned int checkStatus(const char *fnc, FT_STATUS returnStatus);
	bool parseError(char *orgMsg, int *errCode, char errMsg[]);
	asynStatus getLastError(int *errCode);
	asynStatus getValue(const char *cmd, char *rdBuf, unsigned int rdLen);
	asynStatus setValue(const char *cmd, epicsInt32 value);
	asynStatus getValue(const char *cmd, epicsInt32 *value);
	asynStatus setValue(const char *cmd, epicsFloat64 value);
	asynStatus getValue(const char *cmd, epicsFloat64 *value);
	asynStatus sendData(char *wrBuf, unsigned int wrLen);
	asynStatus recvData(char *wrBuf, unsigned int wrLen, char *rdBuf, unsigned int rdLen);
	asynStatus handleError(int function, epicsInt32 value, epicsInt32 changeBit, epicsInt32 valueBit);

	epicsEventId mDeviceEvent;
	unsigned int mFinish;

	// FT instrument handle
	FT_HANDLE mInstr;
	char mManufacturerName[TLDC3100_BUFFER_SIZE];
	char mDeviceName[TLDC3100_BUFFER_SIZE];
	char mSerialNumber[TLDC3100_BUFFER_SIZE];
	char mWantSerialNumber[TLDC3100_BUFFER_SIZE];
	char mFirmwareVersion[TLDC3100_BUFFER_SIZE];
	epicsUInt16 mUsbVID;
	epicsUInt16 mUsbPID;
	int mPosition;
};

#define NUM_TL_DET_PARAMS (&LAST_COMMAND - &FIRST_COMMAND + 1)

#endif //TLDC3100_H

